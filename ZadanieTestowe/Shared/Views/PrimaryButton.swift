//
//  PrimaryButton.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//

import UIKit
class PrimaryButton: UIButton {
    init() {
        super.init(frame: .zero)
        self.backgroundColor = Theme.Colors.Primary
        self.setTitleColor(UIColor.white, for: .normal)
        [UIControl.State.highlighted, UIControl.State.selected, UIControl.State.focused]
            .forEach { [weak self] state in self?.setTitleColor(UIColor.white.withAlphaComponent(0.66), for: state) }
        self.layer.cornerRadius = 5.0
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
