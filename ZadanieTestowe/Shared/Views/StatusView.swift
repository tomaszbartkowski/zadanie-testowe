//
//  StatusView.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//
import EasyPeasy
import Then
import RxSwift
import RxCocoa
class StatusView: UIView {
    
    private let height: CGFloat = 24
    
    private let statusLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        $0.textColor = UIColor.systemGreen
    }
    private let _isCorrect = BehaviorSubject<Bool>(value: true)
    var value: AnyObserver<Bool>
    
    private let disposeBag = DisposeBag()
    
    init() {
        value = _isCorrect.asObserver()
        super.init(frame: .zero)
        self.layer.cornerRadius = height / 2
        addSubview(statusLabel)
        applyConstraints()
        bind()
    }
    
    private func bind() {
        _isCorrect
            .map {  $0 ? "Poprawny" : "Niepoprawny" }
            .bind(to: statusLabel.rx.text)
            .disposed(by: disposeBag)
        
        _isCorrect
            .map { $0 ? UIColor.systemGreen : UIColor.systemRed }
            .bind(to: statusLabel.rx.textColor)
            .disposed(by: disposeBag)

        _isCorrect
            .map { $0 ? UIColor.systemGreen.withAlphaComponent(0.3) : UIColor.systemRed.withAlphaComponent(0.3) }
            .bind(to: rx.backgroundColor)
            .disposed(by: disposeBag)
    }
    
    private func applyConstraints() {
        easy.layout(Height(height))
        statusLabel.easy.layout(CenterY(), Left(12), Right(12))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
