//
//  AppCoordinator.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import UIKit
import RxSwift
import Swinject
class AppCoordinator: BaseCoordinator<Void> {

    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    override func start() -> Observable<CoordinationResult> {
        let navigationController = UINavigationController()
        let pathCoordinator = PathCoordinator(navigationController: navigationController)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        return coordinate(to: pathCoordinator)
    }
    
}
