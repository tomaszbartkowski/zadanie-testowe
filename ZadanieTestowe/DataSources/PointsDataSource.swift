//
//  PointsDataSource.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import RxSwift
import ObjectMapper
protocol PointsDataSourceProtocol {
    func getPoints() -> Observable<[Point]>
}

class PointsLocalDataSource: PointsDataSourceProtocol {
    
    private let FILE_NAME = "path.json"
    
    func getPoints() -> Observable<[Point]> {
        let pointsArray = Mapper<Point>().mapArray(JSONfile: FILE_NAME) ?? []
        return Observable.just(pointsArray)
    }
}
