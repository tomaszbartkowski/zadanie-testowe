//
//  File.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import UIKit
public struct Theme {
    enum Spacing {
        static let HorizontalMargin: CGFloat = 16.0
    }
    
    enum Colors {
        static let Primary: UIColor = UIColor.systemBlue
    }
}
