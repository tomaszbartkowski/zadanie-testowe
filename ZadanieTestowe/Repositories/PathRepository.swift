//
//  PathRepository.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import RxSwift
import RxCocoa

typealias PathRepositoryProtocol = Repository<Path>
final class PathRepository: PathRepositoryProtocol  {
    
    private let dataSource: PointsDataSourceProtocol
    
    private let disposeBag = DisposeBag()
    
    private let path = BehaviorRelay<Path?>(value: nil)
    
    init(dataSource: PointsDataSourceProtocol) {
        self.dataSource = dataSource
        super.init()
        load()
    }
    
    override func subscribeLatest() -> Observable<Path?> {
        return path.asObservable()
    }
    
    override func load() {
        dataSource.getPoints()
            .map { Path(points: $0) }
            .bind(to: path)
            .disposed(by: disposeBag)
            
    }
    
}
