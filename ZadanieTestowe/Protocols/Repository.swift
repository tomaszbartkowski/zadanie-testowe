//
//  Repository.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import RxSwift
import ObjectMapper
protocol RepositoryProtocol {
    associatedtype T
    func subscribeLatest() -> Observable<T?> 
    func load()
}

//see https://github.com/Swinject/Swinject/issues/223
class Repository<T>: RepositoryProtocol {
    
    func subscribeLatest() -> Observable<T?> {
        fatalError()
    }
    
    func load() {
        fatalError()
    }
    
}
