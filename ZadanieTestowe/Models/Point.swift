//
//  Point.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import ObjectMapper
struct Point: Mappable {
    
    var longitude: Double?
    var latitude: Double?
    var distance: Double?
    var accuracy: Double?
    var timestamp: Int?
    var altitude: Double?
    var isCorrect: Bool?
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        
        let transformDouble = TransformOf<Double, String>(fromJSON: { (value: String?) -> Double? in
            return Double(value!)
        }, toJSON: { (value: Double?) -> String? in
            if let value = value {
                return String(value)
            }
            return nil
        })
        
        let transformInt = TransformOf<Int, String>(fromJSON: { (value: String?) -> Int? in
            return Int(value!)
        }, toJSON: { (value: Int?) -> String? in
            if let value = value {
                return String(value)
            }
            return nil
        })
        
        longitude <- (map["longitude"], transformDouble)
        latitude <- (map["latitude"], transformDouble)
        distance <- (map["distance"], transformDouble)
        accuracy <- (map["accuracy"], transformDouble)
        timestamp <- (map["timestamp"], transformInt)
        altitude <- (map["altitude"], transformDouble)
    }
    
    init(_ point: Point, isCorrect: Bool) {
        self.longitude = point.longitude
        self.latitude = point.latitude
        self.distance = point.distance
        self.accuracy = point.accuracy
        self.timestamp = point.timestamp
        self.altitude = point.altitude
        self.isCorrect = isCorrect
     
    }
}
