//
//  Path.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import RxSwift
struct Path {
    var points: [Point]
    
    init(points: [Point]) {
        let deltaDistancesSum = zip(points.compactMap { $0.distance }.suffix(from: 1), points.compactMap { $0.distance })
            .map { $0.0 - $0.1}
            .reduce(0, +)
        
        let averageDeltaDistance = deltaDistancesSum / Double(points.count)
        
         self.points = zip(points.suffix(from: 1), points)
            .map { Point($0.0, isCorrect: ($0.0.distance ?? 0) - ($0.1.distance ?? 0) <= averageDeltaDistance) }
    }
}

