//
//  BaseViewController.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import RxSwift
import Then
class BaseViewController: UIViewController {

    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.systemBackground
        

    }
}
