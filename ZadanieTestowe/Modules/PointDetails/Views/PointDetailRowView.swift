//
//  PointDetailRowView.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//

import EasyPeasy
import Then
import RxSwift
import RxCocoa
class PointDetailRowView: UIView {
    
    private let headerLabel = UILabel().then {
        $0.textColor = UIColor.secondaryLabel
        $0.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    private let valueLabel = UILabel().then {
        $0.textColor = UIColor.label
        $0.font = UIFont.systemFont(ofSize: 20, weight: .medium)
    }
    
    private let separator = UIView().then {
        $0.backgroundColor = UIColor.systemGray5
    }
    
    var value: Binder<String?>
    
    init(title: String?) {
        self.value = valueLabel.rx.text
        super.init(frame: .zero)
        self.addSubview(headerLabel)
        self.addSubview(valueLabel)
        self.addSubview(separator)
        headerLabel.text = title
        applyConstraints()
             
    }
    
    private func applyConstraints() {
        headerLabel.easy.layout(Top(), Left(), Right())
        valueLabel.easy.layout(Top(5).to(headerLabel), Left(), Right())
        separator.easy.layout(Left(), Top(10).to(valueLabel), Right(), Bottom(), Height(1))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
