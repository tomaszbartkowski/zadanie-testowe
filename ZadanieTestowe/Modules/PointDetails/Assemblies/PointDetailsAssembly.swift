//
//  PointDetailsAssembly.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//

import Swinject
class PointDetailsAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(PointDetailsViewModelProtocol.self) { _ in PointDetailViewModel() }
    }
    
}
