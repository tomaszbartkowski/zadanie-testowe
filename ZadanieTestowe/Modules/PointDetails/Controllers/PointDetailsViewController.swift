//
//  PointDetailsViewController.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//
import RxSwift
import RxCocoa
import Then
import EasyPeasy
import MapKit
class PointDetailsViewController: BaseViewController {
    
    private let mapView = MKMapView()
    private let contentContainer = UIView()
    
    private let latDetail = PointDetailRowView(title: "Długość geograficzna")
    private let lngDetail = PointDetailRowView(title: "Szerokość geograficzna")
    private let altDetail = PointDetailRowView(title: "Wysokość n. p. m.")
    private let accuracyDetail = PointDetailRowView(title: "Trafność")
    private let distanceDetail = PointDetailRowView(title: "Dystans")
    private let timestampDetail = PointDetailRowView(title: "Czas")
    private let statusView = StatusView()
    
    private let stackView = UIStackView().then {
        $0.distribution = .equalSpacing
        $0.axis = .vertical
        $0.spacing = 10
    }
    
    private let scrollView = UIScrollView().then {
        $0.alwaysBounceVertical = true
    }
    
    var viewModel: PointDetailsViewModelProtocol! {
        didSet {
            bind()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        applyConstraints()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.didClose.onNext(())
    }
    
    private func bind() {
        viewModel
            .point
            .asObservable()
            .subscribe(onNext: { [unowned self] point in
                let annotation = MKPointAnnotation()
                annotation.coordinate = point
                self.mapView.addAnnotation(annotation)
                self.mapView.showAnnotations([annotation], animated: false)
                
            })
            .disposed(by: disposeBag)

        viewModel
            .accuracy
            .drive(accuracyDetail.value)
            .disposed(by: disposeBag)
        
        viewModel
            .latitude
            .drive(latDetail.value)
            .disposed(by: disposeBag)
        
        viewModel
            .longitude
            .drive(lngDetail.value)
            .disposed(by: disposeBag)
        
        viewModel
            .altitude
            .drive(altDetail.value)
            .disposed(by: disposeBag)
        
        viewModel
            .distance
            .drive(distanceDetail.value)
            .disposed(by: disposeBag)
        
        viewModel
            .timestamp
            .drive(timestampDetail.value)
            .disposed(by: disposeBag)
        
        viewModel
            .isCorrect
            .asObservable()
            .bind(to: statusView.value)
            .disposed(by: disposeBag)

    }
    
    private func configureView() {
        self.navigationItem.title = "Punkt"

        self.view.addSubview(scrollView)
        self.scrollView.addSubview(contentContainer)
        
        contentContainer.addSubview(mapView)

        contentContainer.addSubview(statusView)
        contentContainer.addSubview(stackView)
        
        [latDetail, lngDetail, altDetail, accuracyDetail, distanceDetail, timestampDetail]
            .forEach { [unowned self] view in self.stackView.addArrangedSubview(view) }
    }
    
    private func applyConstraints() {        
        let topConstraint = scrollView.topAnchor.constraint(equalToSystemSpacingBelow: self.view.safeAreaLayoutGuide.topAnchor, multiplier: 1.0)
        topConstraint.constant = 0
        topConstraint.isActive = true
        
        scrollView.easy.layout(Left(), Right(), Bottom())
        contentContainer.easy.layout(Top(), Width().like(self.view), Bottom())
        mapView.easy.layout(Height(220), Left(), Right(), Top())
        
        statusView.easy.layout(Left(Theme.Spacing.HorizontalMargin), Top(20).to(mapView))
        stackView.easy.layout(Left(Theme.Spacing.HorizontalMargin), Top(20).to(statusView), Right(Theme.Spacing.HorizontalMargin), Bottom(20))
    }
    
}
