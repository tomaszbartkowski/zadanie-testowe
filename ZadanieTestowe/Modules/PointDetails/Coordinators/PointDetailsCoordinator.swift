//
//  PointDetailsCoordinator.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//
import RxSwift
import Swinject
class PointDetailsCoordinator: BaseCoordinator<Void> {
 
    let rootViewController: UINavigationController
    
    private let assembler =  Assembler([PointDetailsAssembly()])
    private let model: Point
    
    init(navigationController: UINavigationController, point: Point) {
        self.rootViewController = navigationController
        self.model = point
    }
    
    override func start() -> Observable<Void> {
        let pointDetailViewController = PointDetailsViewController()
        pointDetailViewController.view.frame = UIScreen.main.bounds
        
        let viewModel = assembler.resolver.resolve(PointDetailsViewModelProtocol.self)!
        viewModel.configureWithPoint(model)
        pointDetailViewController.viewModel = viewModel
        
        rootViewController.pushViewController(pointDetailViewController, animated: true)
        
        return viewModel.didClose.take(1)

    }
   
}
