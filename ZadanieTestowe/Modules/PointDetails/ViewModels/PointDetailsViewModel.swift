//
//  PointDetailsViewModel.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 26/02/2021.
//
import RxSwift
import RxCocoa
import CoreLocation
protocol PointDetailsViewModelProtocol {
    func configureWithPoint(_ point: Point)
    
    var point: Driver<CLLocationCoordinate2D> { get }
    var latitude: Driver<String?> { get }
    var longitude: Driver<String?> { get }
    var accuracy: Driver<String?> { get }
    var timestamp: Driver<String?> { get }
    var altitude: Driver<String?> { get }
    var distance: Driver<String?> { get }
    var isCorrect: Driver<Bool> { get }
    
    var didClose: PublishSubject<Void> { get }
}

class PointDetailViewModel: PointDetailsViewModelProtocol {
    
    private let _point = BehaviorRelay<Point?>(value: nil)
    
    let didClose = PublishSubject<Void>()
    
    var point: Driver<CLLocationCoordinate2D>  {
        _point
            .filter { $0 != nil}
            .map { $0! }
            .map { (lat: $0.latitude ?? 0, lng: $0.longitude ?? 0) }
            .map { CLLocationCoordinate2D(latitude: $0.lat, longitude: $0.lng) }
            .asDriver(onErrorJustReturn: CLLocationCoordinate2D())
    }
    
    var latitude: Driver<String?>{
        _point
            .map { String($0?.latitude ?? 0 ) }
            .asDriver(onErrorJustReturn: "")
    }
    
    var longitude: Driver<String?>{
        _point
            .map { String($0?.longitude ?? 0 ) }
            .asDriver(onErrorJustReturn: "")
    }
    
    var altitude: Driver<String?>{
        _point
            .map { String($0?.altitude ?? 0 ) }
            .asDriver(onErrorJustReturn: "")
    }
    
    var distance: Driver<String?>{
        _point
            .map { String($0?.distance ?? 0 ) }
            .asDriver(onErrorJustReturn: "")
    }
    
    var accuracy: Driver<String?>{
        _point
            .map { String($0?.accuracy ?? 0 ) }
            .asDriver(onErrorJustReturn: "")
    }
    
    var timestamp: Driver<String?>{
        _point
            .map { String($0?.timestamp ?? 0 ) }
            .asDriver(onErrorJustReturn: "")
    }
    
    var isCorrect: Driver<Bool> {
        _point
            .map { $0?.isCorrect ?? false  }
            .asDriver(onErrorJustReturn: false)
    }
    
    func configureWithPoint(_ point: Point) {
        _point.accept(point)
    }
    
}
