//
//  MapViewController.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import RxCocoa
import RxSwift
import MapKit
import Then
import EasyPeasy
class MapViewController: BaseViewController {
    
    var viewModel: PathViewModelProtocol! {
        didSet {
            bind()
        }
    }
    
    private let toggleButton = PrimaryButton()
    
    private var overlay: MKOverlay?
    private var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeMap()
        configureViews()
        applyConstraints()
    }
    
    private func bind() {
        viewModel
            .currentPoints
            .asObservable()
            .filter { !$0.isEmpty }
            .map { $0.map { CLLocationCoordinate2D(latitude: $0.latitude ?? 0, longitude: $0.longitude ?? 0) } }
            .map {  MKPolyline(coordinates: $0, count: $0.count) }
            .subscribe(onNext: { [unowned self] polyline in
                if let overlay = self.overlay {
                    self.mapView.removeOverlay(overlay)
                }
                self.mapView.addOverlay(polyline, level: MKOverlayLevel.aboveRoads)
                self.mapView.setVisibleMapRect(polyline.boundingMapRect, edgePadding: UIEdgeInsets(top: 30, left: 20, bottom: 180, right: 20), animated: true)
                self.overlay = polyline
            })
            .disposed(by: disposeBag)
        
        toggleButton
            .rx
            .tap
            .asObservable()
            .withLatestFrom(viewModel.selectedOnlyCorrect)
            .map { !$0 }
            .bind(to: viewModel.selectedOnlyCorrect)
            .disposed(by: disposeBag)
            
        viewModel
            .selectedOnlyCorrect
            .map { $0 ? "Poprawne" : "Wszystkie" }
            .asDriver(onErrorJustReturn: "")
            .drive(toggleButton.rx.title())
            .disposed(by: disposeBag)

    }
    
    private func initializeMap() {
        mapView = MKMapView(frame: view.bounds)
        mapView.delegate = self
        self.view.addSubview(mapView)
    }
    
    private func configureViews() {
        self.view.addSubview(toggleButton)
    }
    
    private func applyConstraints() {
        let topConstraint = toggleButton.topAnchor.constraint(equalToSystemSpacingBelow: self.view.safeAreaLayoutGuide.topAnchor, multiplier: 1.0)
        topConstraint.constant = 20
        topConstraint.isActive = true
        toggleButton.easy.layout(Right(Theme.Spacing.HorizontalMargin), Height(40), Width(110))
    }
    
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4
        return renderer
    }
}
