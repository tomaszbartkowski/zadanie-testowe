//
//  PointsListViewController.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import RxCocoa
import RxSwift
import Pulley
import Then
import EasyPeasy
class PointsListViewController: BaseViewController {
    
    private static let CellIdentifier = "PointTableViewCell"
    
    private let tableView = UITableView().then {
        $0.backgroundColor = .systemBackground
        $0.separatorStyle = .none
        $0.rowHeight = UITableView.automaticDimension
        $0.estimatedRowHeight = UITableView.automaticDimension
        $0.register(PointTableViewCell.self, forCellReuseIdentifier: CellIdentifier)
    }
    
    private let segmentedControl = UISegmentedControl(items: ["Poprawne" ,"Niepoprawne"]).then {
        $0.selectedSegmentIndex = 0
    }
    
    var viewModel: PathViewModelProtocol! {
        didSet {
            bind()
        }
    }
    
    private var drawerBottomSafeArea: CGFloat = 0.0 {
        didSet {
            tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: drawerBottomSafeArea, right: 0.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        applyConstraints()
    }
    
    private func bind() {
        
        viewModel
            .pointCellViewModels
            .drive(tableView.rx.items(cellIdentifier: PointsListViewController.CellIdentifier, cellType: PointTableViewCell.self)) { (_, element, cell) in
                cell.viewModel = element
            }
            .disposed(by: disposeBag)
        
        segmentedControl.rx.selectedSegmentIndex
            .map { $0 == 0 }
            .bind(to: viewModel.selectedCorrectOnSegment)
            .disposed(by: disposeBag)
        
        tableView
            .rx.modelSelected(PointCellViewModel.self)
            .flatMap { $0.point.asObservable() }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: viewModel.didSelectPoint)
            .disposed(by: disposeBag)
        
        tableView
            .rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.tableView.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: disposeBag)

    }
    
    private func configureViews() {
        self.view.addSubview(tableView)
        self.view.addSubview(segmentedControl)
    }
    
    private func applyConstraints() {
        tableView.easy.layout(Top(10).to(segmentedControl), Left(), Right(), Bottom())
        
        segmentedControl.easy.layout(CenterX())
        segmentedControl.easy.layout(Top(20), Left(40), Right(40), Height(32))
    }
    
}

extension PointsListViewController: PulleyDrawerViewControllerDelegate {

    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return 130 + (pulleyViewController?.currentDisplayMode == .drawer ? drawerBottomSafeArea : 0.0)
    }
    
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [.open, .collapsed]
    }
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        drawerBottomSafeArea = bottomSafeArea > 0 ? 16 : 0

        tableView.isScrollEnabled = drawer.drawerPosition == .open || drawer.currentDisplayMode == .panel
        if drawer.drawerPosition == .collapsed {
            tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
}
