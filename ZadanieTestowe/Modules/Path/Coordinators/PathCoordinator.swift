//
//  PathCoordinator.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//
import RxSwift
import RxCocoa
import Swinject
import Pulley
class PathCoordinator: BaseCoordinator<Void> {
 
    let rootViewController: UINavigationController
    
    private let assembler = Assembler([PathAssembly()])
    
    init(navigationController: UINavigationController) {
        self.rootViewController = navigationController
    }
    
    override func start() -> Observable<Void> {
        let mapViewController = MapViewController()
        mapViewController.view.frame = UIScreen.main.bounds
        
        let pointsListViewController = PointsListViewController()
        pointsListViewController.view.frame = UIScreen.main.bounds
        
        let pulleyController = PulleyViewController(contentViewController: mapViewController, drawerViewController: pointsListViewController)
        pulleyController.navigationItem.title = "Trasa"
        
        let viewModel = assembler.resolver.resolve(PathViewModelProtocol.self)!
        mapViewController.viewModel = viewModel
        pointsListViewController.viewModel = viewModel
        
        viewModel
            .didSelectPoint
            .flatMap { [weak self] point in self?.coordinateToPointDetails(point: point) ?? Observable.empty() }
            .subscribe()
            .disposed(by: disposeBag)
        
        rootViewController.setViewControllers([pulleyController], animated: false)
        
        return Observable.never()
    }
    
    private func coordinateToPointDetails(point: Point) -> Observable<Void> {
        let pointDetailsCoordinator = PointDetailsCoordinator(navigationController: rootViewController, point: point)
        return coordinate(to: pointDetailsCoordinator)
    }

}
