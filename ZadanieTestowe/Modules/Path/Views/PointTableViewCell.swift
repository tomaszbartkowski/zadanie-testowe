//
//  PointTableViewCell.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import RxSwift
import RxCocoa
import Then
import EasyPeasy
class PointTableViewCell: BaseTableViewCell {
    
    private let coordinatesLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        $0.textColor = UIColor.label
    }
    
    private let accuracyLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 15)
        $0.textColor = UIColor.secondaryLabel
    }
    
    private let statusView = StatusView()
    
    var viewModel: PointCellViewModel! {
        didSet {
            bind()
        }
    }
    
    private func bind() {
        viewModel
            .point
            .map { "\(String($0?.latitude ?? 0)), \(String($0?.longitude ?? 0))" }
            .drive(coordinatesLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .point
            .map { "Trafność: \(String($0?.longitude ?? 0))" }
            .drive(accuracyLabel.rx.text)
            .disposed(by: disposeBag)

        viewModel
            .point
            .map { $0?.isCorrect ?? false }
            .asObservable()
            .bind(to: statusView.value)
            .disposed(by: disposeBag)
        
//        viewModel
//            .point
//            .map { ($0?.isCorrect ?? false) ? "Poprawny" : "Niepoprawny" }
//            .drive(statusLabel.rx.text)
//            .disposed(by: disposeBag)
//
//        viewModel
//            .point
//            .map { ($0?.isCorrect ?? false) ? UIColor.systemGreen : UIColor.systemRed }
//            .drive(statusLabel.rx.textColor)
//            .disposed(by: disposeBag)
//
//        viewModel
//            .point
//            .map { ($0?.isCorrect ?? false) ? UIColor.systemGreen.withAlphaComponent(0.3) : UIColor.systemRed.withAlphaComponent(0.3) }
//            .drive(statusContainer.rx.backgroundColor)
//            .disposed(by: disposeBag)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(coordinatesLabel)
        addSubview(accuracyLabel)
        addSubview(statusView)

        
        applyConstraints()
    }
    
    private func applyConstraints() {
        coordinatesLabel.easy.layout(Top(15), Left(Theme.Spacing.HorizontalMargin), Right(Theme.Spacing.HorizontalMargin))
        accuracyLabel.easy.layout(Top(5).to(coordinatesLabel), Left().to(coordinatesLabel, .left), Right().to(coordinatesLabel, .right), Bottom(15))
        statusView.easy.layout(Top(15), Right().to(coordinatesLabel, .right))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
