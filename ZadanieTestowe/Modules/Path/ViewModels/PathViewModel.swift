//
//  PathViewModel.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import RxSwift
import RxCocoa
protocol PathViewModelProtocol {
    var currentPoints: Driver<[Point]> { get }
    var selectedOnlyCorrect: BehaviorSubject<Bool> { get }
    var selectedCorrectOnSegment: BehaviorSubject<Bool> { get }
    var pointCellViewModels: Driver<[PointCellViewModel]> { get }
    var didSelectPoint: PublishSubject<Point> { get }
}

class PathViewModel: PathViewModelProtocol {
    
    private let disposeBag = DisposeBag()
    
    let selectedOnlyCorrect = BehaviorSubject<Bool>(value: true)
    let selectedCorrectOnSegment = BehaviorSubject<Bool>(value: true)
    let didSelectPoint = PublishSubject<Point>()
    
    private let path = BehaviorRelay<Path?>(value: nil)
    
    var currentPoints: Driver<[Point]>  {
        return Observable.combineLatest( self.path.map { $0?.points ?? [] }, self.selectedOnlyCorrect )
            .map { tuple in tuple.0.filter { tuple.1 ? ($0.isCorrect ?? false) : true } }
            .asDriver(onErrorJustReturn: [])
    }
    
    var pointCellViewModels: Driver<[PointCellViewModel]>  {
        return Observable.combineLatest( self.path.map { $0?.points ?? [] }, self.selectedCorrectOnSegment )
            .map { tuple in tuple.0.filter { ($0.isCorrect ?? false) == tuple.1 } }
            .map { $0.map { PointCellViewModel($0) } }
            .asDriver(onErrorJustReturn: [])
    }

    private let pathRepository: PathRepositoryProtocol
    
    init(pathRepository: PathRepositoryProtocol) {
        self.pathRepository = pathRepository
        bind()
    }
    
    private func bind() {
        self.pathRepository
            .subscribeLatest()
            .bind(to: path)
            .disposed(by: disposeBag)
    }
    
}
