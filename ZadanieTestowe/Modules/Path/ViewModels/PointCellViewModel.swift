//
//  PointCellViewModel.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import RxSwift
import RxCocoa
class PointCellViewModel {
    
    var point: Driver<Point?> {
        return _point.asDriver()
    }
    
    private let _point = BehaviorRelay<Point?>(value: nil)
    
    init(_ point: Point) {
        _point.accept(point)
    }
    
}
