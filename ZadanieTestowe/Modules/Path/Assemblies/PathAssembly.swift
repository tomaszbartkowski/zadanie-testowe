//
//  PathAssembly.swift
//  ZadanieTestowe
//
//  Created by Tomasz Bartkowski on 25/02/2021.
//

import Swinject
class PathAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(PointsDataSourceProtocol.self) { _ in PointsLocalDataSource() }
        container.register(PathRepositoryProtocol.self) { r in PathRepository(dataSource: r.resolve(PointsDataSourceProtocol.self)!) }
        container.register(PathViewModelProtocol.self) { r in PathViewModel(pathRepository: r.resolve(PathRepositoryProtocol.self)!) }
            .inObjectScope(.weak)
    }
    
}
